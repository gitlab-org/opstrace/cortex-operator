package controllers

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/ginkgo/extensions/table"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = Describe("helpers", func() {
	var name = "test"

	Context("WithPodAntiAffinityTerm", func() {
		DescribeTable("should add the correct anti-affinity term",
			func(expected *corev1.Affinity, affinity *corev1.Affinity) {
				result := WithPodAntiAffinity(name, affinity)
				Expect(result).To(Equal(expected))
			},

			Entry(
				"when affinity is nil",
				&corev1.Affinity{
					PodAntiAffinity: &corev1.PodAntiAffinity{
						PreferredDuringSchedulingIgnoredDuringExecution: []corev1.WeightedPodAffinityTerm{
							{
								PodAffinityTerm: corev1.PodAffinityTerm{
									LabelSelector: &v1.LabelSelector{
										MatchLabels: map[string]string{"name": name},
									},
									TopologyKey: "kubernetes.io/hostname",
								},
								Weight: 100,
							},
						},
					},
				},
				nil,
			),

			Entry(
				"when pod anti-affinity is nil",
				&corev1.Affinity{
					NodeAffinity: &corev1.NodeAffinity{},
					PodAffinity:  &corev1.PodAffinity{},
					PodAntiAffinity: &corev1.PodAntiAffinity{
						PreferredDuringSchedulingIgnoredDuringExecution: []corev1.WeightedPodAffinityTerm{
							{
								PodAffinityTerm: corev1.PodAffinityTerm{
									LabelSelector: &v1.LabelSelector{
										MatchLabels: map[string]string{"name": name},
									},
									TopologyKey: "kubernetes.io/hostname",
								},
								Weight: 100,
							},
						},
					},
				},
				&corev1.Affinity{
					NodeAffinity: &corev1.NodeAffinity{},
					PodAffinity:  &corev1.PodAffinity{},
				},
			),
			Entry(
				"when pod anti-affinity is set",
				&corev1.Affinity{
					PodAntiAffinity: &corev1.PodAntiAffinity{
						PreferredDuringSchedulingIgnoredDuringExecution: []corev1.WeightedPodAffinityTerm{
							{
								PodAffinityTerm: corev1.PodAffinityTerm{
									Namespaces: []string{"test"},
								},
							},
							{
								PodAffinityTerm: corev1.PodAffinityTerm{
									LabelSelector: &v1.LabelSelector{
										MatchLabels: map[string]string{"name": name},
									},
									TopologyKey: "kubernetes.io/hostname",
								},
								Weight: 100,
							},
						},
					},
				},
				&corev1.Affinity{
					PodAntiAffinity: &corev1.PodAntiAffinity{
						PreferredDuringSchedulingIgnoredDuringExecution: []corev1.WeightedPodAffinityTerm{
							{
								PodAffinityTerm: corev1.PodAffinityTerm{
									Namespaces: []string{"test"},
								},
							},
						},
					},
				},
			),
		)
	})
})
